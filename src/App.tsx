import React from 'react';
import {SideMenu} from './components/SideMenu';
import {TopMenu} from './components/TopMenu';
// import './App.css';

const App: React.FC = () => {
  return (
    <div className="App">
      <SideMenu />
      <TopMenu />
    </div>
  );
}

export default App;
