import React, { Component } from 'react';
import { Image, Input, Menu, MenuItemProps, Dropdown } from 'semantic-ui-react';
import CSS from 'csstype';

const menuStyle: CSS.Properties = {
  position: 'relative',
  left: '210px'
}

const searchStyle: CSS.Properties = {
  left: '400px',
  width: '700px'
}

const dropdownStyle: CSS.Properties = {
  marginRight: '215px'
}


type ActiveItem = {
  activeItem: any
}

export class TopMenu extends Component<{}, ActiveItem> {
  render() {
    return (
      <Menu fixed={'top'} style={menuStyle}>
        <Menu.Item style={searchStyle}>
          <Input className='icon' icon='search' placeholder='Search...' />
        </Menu.Item>

        <Menu.Menu position='right' style={dropdownStyle}>

          <Dropdown item text='Username'>
            <Dropdown.Menu>
            {/* <Image src={process.env.PUBLIC_URL + "img.jpeg"} size="small" /> */}
              <Dropdown.Item>Logout</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </Menu.Menu>

      </Menu>
    )
  }
}
