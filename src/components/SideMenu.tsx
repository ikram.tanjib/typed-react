import React, { Component } from 'react';
import { Container, Divider, Image, Menu, MenuItemProps } from 'semantic-ui-react';
import CSS from 'csstype';

const menuStyle: CSS.Properties = {
  float: 'left',
  position: 'absolute'
}


type ActiveItem = {
  activeItem: any
}

export class SideMenu extends Component<{}, ActiveItem> {


  handleItemClick = (e: React.MouseEvent<HTMLAnchorElement>, name: MenuItemProps) => this.setState({ activeItem: name })

  constructor(props: ActiveItem) {
    super(props);
    // this.state = { activeItem: 'inbox' };
    this.state = { activeItem: this.props };
  }

  render() {
    const { activeItem } = this.state;
    return (
        <Menu vertical style={menuStyle}>
          <Menu.Item>
            <div>
              <Image
                src="star_wars_logo_black.png"
                as="a"
                href="#"
                size="tiny" />
            </div>
          </Menu.Item>

          <Menu.Item
            name='dashboard'
            active={activeItem === 'dashboard'}
            onClick={this.handleItemClick}
          >
            Dashboard
        </Menu.Item>

          <Menu.Item
            name='star-wars'
            active={activeItem === 'star-wars'}
            onClick={this.handleItemClick}
          >
            Star Wars
        </Menu.Item>

          <Menu.Item
            name='avengers'
            active={activeItem === 'avengers'}
            onClick={this.handleItemClick}
          >
            Avengers
        </Menu.Item>
          {/* <Menu.Item>
          <Input icon='search' placeholder='Search mail...' />
        </Menu.Item> */}
        </Menu>
    )
  }
}